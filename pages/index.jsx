import { useQuery } from "@apollo/react-hooks";
import { LazyLoadImage } from 'react-lazy-load-image-component';
import Reveal from 'react-awesome-reveal';

import ALink from '~/components/features/alink';
import OwlCarousel from '~/components/features/owl-carousel';
import NewCollection from '~/components/partials/home/new-collection';
import TopCollection from "~/components/partials/home/top-collection";
import BlogCollection from '~/components/partials/home/blog-collection';
import NewsletterModal from "~/components/features/modals/newsletter-modal";

import withApollo from '~/server/apollo';
import { GET_HOME_DATA } from '~/server/queries';

import { introSlider, fadeInUpShorter, fadeInLeftShorter, fadeInRightShorter, fadeIn } from '~/utils/data';

function Home() {
	const { data, loading, error } = useQuery(GET_HOME_DATA);
	const products = data && data.homeData.products;
	const posts = data && data.homeData.posts;

	if (error) {
		return <div></div>
	}

	return (
		<main className="main home-page skeleton-body">
			<div className="">
				<div className="intro-slider-container slider-container-ratio mb-2">
					<OwlCarousel adClass="intro-slider owl-simple owl-light owl-nav-inside" options={introSlider}>
						<div className="intro-slide">
							<figure className="slide-image">
								<picture>
									<img src="images/home/sliders/slide-1.jpg" alt="banner" />
								</picture>
							</figure>

							<div className="intro-content">
								<Reveal keyframes={fadeInUpShorter} delay={100} duration={1000}>
									<>
										<h3 className="intro-subtitle text-dark">Պայուսակներ</h3>
										<h1 className="intro-title text-dark">Պայուսակներ կանանց համար</h1>

										<div className="intro-price text-dark">Կոշիկի և պայուսակների մեծ տեսականի</div>

										<ALink href="/shop/sidebar/list" className="btn btn-white-primary btn-round">
											<span>Դիտել ավելին</span>
											<i className="icon-long-arrow-right"></i>
										</ALink>
									</>
								</Reveal>
							</div>
						</div>

						<div className="intro-slide">
							<figure className="slide-image">
								<picture>
									<img src="images/home/sliders/slide-2.jpg" alt="banner" />
								</picture>
							</figure>

							<div className="intro-content">
								<Reveal keyframes={fadeInUpShorter} delay={100} duration={1000}>
									<>
										<h3 className="intro-subtitle text-dark">WOMEN SHIRT</h3>
										<h1 className="intro-title text-dark">LUXE SLIM PANT WITH SPLIT</h1>

										{/* <div className="intro-price text-white">from $49.99</div> */}

										<ALink href="/shop/sidebar/list" className="btn btn-white-primary btn-round">
											<span>VIEW MORE</span>
											<i className="icon-long-arrow-right"></i>
										</ALink>
									</>
								</Reveal>
							</div>
						</div>

						<div className="intro-slide">
							<figure className="slide-image">
								<picture>
									<img src="images/home/sliders/slide-3.jpg" alt="banner" />
								</picture>
							</figure>

							<div className="intro-content">
								<Reveal keyframes={fadeInUpShorter} delay={100} duration={1000}>
									<>
										<h3 className="intro-subtitle  text-dark">WOMEN SHIRT</h3>
										<h1 className="intro-title text-dark">SWING T-SHIRT</h1>

										{/* <div className="intro-price text-white">starting at 60% off</div> */}

										<ALink href="/shop/sidebar/list" className="btn btn-white-primary btn-round">
											<span>VIEW MORE</span>
											<i className="icon-long-arrow-right"></i>
										</ALink>
									</>
								</Reveal>
							</div>
						</div>
					</OwlCarousel>
				</div>
			</div>

			{/* <div className="banner-group">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8">
                            <div className="row">
                                <div className="col-sm-6">
                                    <Reveal keyframes={ fadeInRightShorter } delay={ 150 } duration={ 1000 } triggerOnce>
                                        <div className="banner banner-overlay banner-1">
                                            <ALink href="/shop/sidebar/3cols" className="lazy-media">
                                                <div className="lazy-overlay"></div>

                                                <LazyLoadImage
                                                    alt="banner"
                                                    src="images/home/banners/banner-1.jpg"
                                                    threshold={ 400 }
                                                    width="376"
                                                    height="auto"
                                                    effect="blur"
                                                />
                                            </ALink>

                                            <div className="banner-content banner-content-right">
                                                <h4 className="banner-subtitle"><ALink href="/shop/sidebar/list">New Arrivals</ALink></h4>
                                                <h3 className="banner-title text-white"><ALink href="/shop/sidebar/list">Sneakers & <br />Athletic Shoes</ALink></h3>
                                                <ALink href="/shop/sidebar/list" className="btn btn-outline-white banner-link btn-round">Discover Now</ALink>
                                            </div>
                                        </div>
                                    </Reveal>
                                </div>

                                <div className="col-sm-6">
                                    <Reveal keyframes={ fadeIn } delay={ 150 } duration={ 1000 } triggerOnce>
                                        <div className="banner banner-overlay banner-overlay-light banner-2">
                                            <ALink href="/shop/sidebar/3cols" className="lazy-media">
                                                <div className="lazy-overlay"></div>

                                                <LazyLoadImage
                                                    alt="banner"
                                                    src="images/home/banners/banner-2.jpg"
                                                    threshold={ 400 }
                                                    width="376"
                                                    height="auto"
                                                    effect="blur"
                                                />
                                            </ALink>

                                            <div className="banner-content">
                                                <h4 className="banner-subtitle bright-black"><ALink href="/shop/sidebar/list">Clearance</ALink></h4>
                                                <h3 className="banner-title"><ALink href="/shop/sidebar/list">Sandals</ALink></h3>
                                                <div className="banner-text"><ALink href="/shop/sidebar/list">up to 70% off</ALink></div>
                                                <ALink href="/shop/sidebar/list" className="btn btn-outline-gray banner-link btn-round">Shop Now</ALink>
                                            </div>
                                        </div>
                                    </Reveal>
                                </div>
                            </div>

                            <Reveal keyframes={ fadeInLeftShorter } delay={ 150 } duration={ 1000 } triggerOnce>
                                <div className="banner banner-large banner-overlay d-none d-sm-block banner-3">
                                    <ALink href="/shop/sidebar/3cols" className="lazy-media">
                                        <div className="lazy-overlay"></div>

                                        <LazyLoadImage
                                            alt="banner"
                                            src="images/home/banners/banner-3.jpg"
                                            threshold={ 400 }
                                            width="376"
                                            height="auto"
                                            effect="blur"
                                        />
                                    </ALink>

                                    <div className="banner-content">
                                        <h4 className="banner-subtitle text-white"><ALink href="/shop/sidebar/list">On Sale</ALink></h4>
                                        <h3 className="banner-title text-white"><ALink href="/shop/sidebar/list">Slip-On Loafers</ALink></h3>
                                        <div className="banner-text text-white"><ALink href="/shop/sidebar/list">up to 30% off</ALink></div>
                                        <ALink href="/shop/sidebar/list" className="btn btn-outline-white banner-link btn-round">Shop Now</ALink>
                                    </div>
                                </div>
                            </Reveal>
                        </div>

                        <div className="col-lg-4 d-sm-none d-lg-block">
                            <Reveal keyframes={ fadeInLeftShorter } delay={ 150 } duration={ 1000 } triggerOnce>
                                <div className="banner banner-middle banner-overlay banner-4">
                                    <ALink href="/shop/sidebar/3cols" className="lazy-media">
                                        <div className="lazy-overlay"></div>

                                        <LazyLoadImage
                                            alt="banner"
                                            src="images/home/banners/banner-4.jpg"
                                            threshold={ 400 }
                                            width="376"
                                            height="auto"
                                            effect="blur"
                                        />
                                    </ALink>

                                    <div className="banner-content banner-content-bottom">
                                        <h4 className="banner-subtitle text-white"><ALink href="/shop/sidebar/list">On Sale</ALink></h4>
                                        <h3 className="banner-title text-white"><ALink href="/shop/sidebar/list">Amazing <br />Lace Up Boots</ALink></h3>
                                        <div className="banner-text text-white"><ALink href="/shop/sidebar/list">from $39.00</ALink></div>
                                        <ALink href="/shop/sidebar/list" className="btn btn-outline-white banner-link btn-round">Discover Now</ALink>
                                    </div>
                                </div>
                            </Reveal>
                        </div>
                    </div>
                </div>
            </div> */}

			{/* <div className="icon-boxes-container icon-boxes-separator bg-transparent">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-6 col-lg-3">
                            <Reveal keyframes={ fadeInRightShorter } delay={ 200 } duration={ 1000 } triggerOnce>
                                <div className="icon-box icon-box-side">
                                    <span className="icon-box-icon text-primary">
                                        <i className="icon-rocket"></i>
                                    </span>
                                    <div className="icon-box-content">
                                        <h3 className="icon-box-title">Free Shipping</h3>

                                        <p>Orders $50 or more</p>
                                    </div>
                                </div>
                            </Reveal>
                        </div>

                        <div className="col-sm-6 col-lg-3">
                            <Reveal keyframes={ fadeInRightShorter } delay={ 200 } duration={ 1000 } triggerOnce>
                                <div className="icon-box icon-box-side">
                                    <span className="icon-box-icon text-primary">
                                        <i className="icon-rotate-left"></i>
                                    </span>

                                    <div className="icon-box-content">
                                        <h3 className="icon-box-title">Free Returns</h3>

                                        <p>Within 30 days</p>
                                    </div>
                                </div>
                            </Reveal>
                        </div>

                        <div className="col-sm-6 col-lg-3">
                            <Reveal keyframes={ fadeInLeftShorter } delay={ 200 } duration={ 1000 } triggerOnce>
                                <div className="icon-box icon-box-side">
                                    <span className="icon-box-icon text-primary">
                                        <i className="icon-info-circle"></i>
                                    </span>

                                    <div className="icon-box-content">
                                        <h3 className="icon-box-title">Get 20% Off 1 Item</h3>

                                        <p>When you sign up</p>
                                    </div>
                                </div>
                            </Reveal>
                        </div>

                        <div className="col-sm-6 col-lg-3">
                            <Reveal keyframes={ fadeInLeftShorter } delay={ 200 } duration={ 1000 } triggerOnce>
                                <div className="icon-box icon-box-side">
                                    <span className="icon-box-icon text-primary">
                                        <i className="icon-life-ring"></i>
                                    </span>

                                    <div className="icon-box-content">
                                        <h3 className="icon-box-title">We Support</h3>

                                        <p>24/7 amazing services</p>
                                    </div>
                                </div>
                            </Reveal>
                        </div>
                    </div>
                </div>
            </div>

            <Reveal keyframes={ fadeIn } delay={ 100 } duration={ 1000 } triggerOnce>
                <NewCollection products={ products } loading={ loading } />
            </Reveal>

            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-sm-6 col-md-4">
                        <Reveal keyframes={ fadeInRightShorter } delay={ 100 } duration={ 1000 } triggerOnce >
                            <div className="banner banner-cat">
                                <ALink href="/shop/sidebar/3cols?category=women-s" className="lazy-media">
                                    <div className="lazy-overlay"></div>

                                    <LazyLoadImage
                                        alt="banner"
                                        src="images/home/banners/banner-5.jpg"
                                        threshold={ 200 }
                                        width="376"
                                        height="auto"
                                        effect="blur"
                                    />
                                </ALink>

                                <div className="banner-content banner-content-overlay text-center">
                                    <h3 className="banner-title font-weight-normal">Women's</h3>
                                    <h4 className="banner-subtitle">125 Products</h4>
                                    <ALink href="/shop/sidebar/3cols?category=women-s" className="banner-link">SHOP NOW</ALink>
                                </div>
                            </div>
                        </Reveal>
                    </div>

                    <div className="col-sm-6 col-md-4">
                        <Reveal keyframes={ fadeIn } delay={ 100 } duration={ 1000 } triggerOnce>
                            <div className="banner banner-cat">
                                <ALink href="/shop/sidebar/3cols?category=men-s" className="lazy-media">
                                    <div className="lazy-overlay"></div>

                                    <LazyLoadImage
                                        alt="banner"
                                        src="images/home/banners/banner-6.jpg"
                                        threshold={ 200 }
                                        width="376"
                                        height="auto"
                                        effect="blur"
                                    />
                                </ALink>

                                <div className="banner-content banner-content-overlay text-center">
                                    <h3 className="banner-title font-weight-normal">Men's</h3>
                                    <h4 className="banner-subtitle">97 Products</h4>
                                    <ALink href="/shop/sidebar/list?category=men-s" className="banner-link">SHOP NOW</ALink>
                                </div>
                            </div>
                        </Reveal>
                    </div>

                    <div className="col-sm-6 col-md-4">
                        <Reveal keyframes={ fadeInLeftShorter } delay={ 100 } duration={ 1000 } triggerOnce>
                            <div className="banner banner-cat">
                                <ALink href="/shop/sidebar/3cols?category=kid-s" className="lazy-media">
                                    <div className="lazy-overlay"></div>

                                    <LazyLoadImage
                                        alt="banner"
                                        src="images/home/banners/banner-7.jpg"
                                        threshold={ 200 }
                                        width="376"
                                        height="auto"
                                        effect="blur"
                                    />
                                </ALink>

                                <div className="banner-content banner-content-overlay text-center">
                                    <h3 className="banner-title font-weight-normal">Kid's</h3>
                                    <h4 className="banner-subtitle">48 Products</h4>
                                    <ALink href="/shop/sidebar/list?category=kid-s" className="banner-link">SHOP NOW</ALink>
                                </div>
                            </div>
                        </Reveal>
                    </div>
                </div>
            </div>

            <div className="mb-4"></div>

            <Reveal keyframes={ fadeIn } delay={ 100 } duration={ 1000 } triggerOnce>
                <TopCollection products={ products } loading={ loading } />
            </Reveal>

            <div className="mb-7"></div>

            <div className="container">
                <div className="cta cta-horizontal cta-horizontal-box bg-image mb-4 mb-lg-6" style={ { backgroundImage: 'url(images/home/bg-1.jpg)' } }>
                    <div className="row flex-column flex-lg-row align-items-lg-center">
                        <div className="col">
                            <h3 className="cta-title text-primary">New Deals! Start Daily at 12pm e.t.</h3>
                            <p className="cta-desc">Get <em className="font-weight-medium">FREE SHIPPING* & 5% rewards</em> on every order with Molla Theme rewards program</p>
                        </div>

                        <div className="col-auto">
                            <ALink href="#" className="btn btn-white-primary btn-round"><span className="text-left">Add to Cart for $50.00/yr</span><i className="icon-long-arrow-right"></i></ALink>
                        </div>
                    </div>
                </div>
            </div>

            <Reveal keyframes={ fadeIn } delay={ 100 } duration={ 1000 } triggerOnce>
                <BlogCollection posts={ posts } loading={ loading } />
            </Reveal>

            <div
                className="cta bg-image bg-dark pt-4 pb-5 mb-0"
                style={ { backgroundImage: 'url(images/home/bg-2.jpg)' } }
            >
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-sm-10 col-md-8 col-lg-6">
                            <div className="cta-heading text-center">
                                <h3 className="cta-title text-white">Subscribe for Our Newsletter</h3>

                                <p className="cta-desc text-white">
                                    and receive
                                <span className="font-weight-normal"> $20 coupon</span> for first shopping
                            </p>
                            </div>

                            <form action="#">
                                <div className="input-group input-group-round">
                                    <input
                                        type="email"
                                        className="form-control form-control-white border-0"
                                        placeholder="Enter your Email Address"
                                        aria-label="Email Adress"
                                        required
                                    />
                                    <div className="input-group-append">
                                        <button className="btn btn-white" type="submit">
                                            <span>Subscribe</span>
                                            <i className="icon-long-arrow-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> */}

			{/* <NewsletterModal /> */}
		</main>
	)
}

export default withApollo({ ssr: typeof window == 'undefined' })(Home);